puts 'This program will create a "room" then add "people" to that room until two people share the same birthday.'
rerun = true

while rerun
  puts 'How many rooms should I create?'
  input = gets.chomp.to_i
  i = 0
  people_total = 0 #total people in all rooms
  people_max = 0 #largest number of people in a single room

  while i < input do
    room = Array.new([])
    match = false

    while !match do
      bDay = 1 + rand(365)
      if room.include? bDay
        room << bDay
        people_total += room.size
        if room.size > people_max
          people_max = room.size
        end
        match = true
      else
        room << bDay
      end
    end
    i+=1
  end

  people_avg = people_total / input

  puts 'The average number of people in each room when a match was reached was ' + people_avg.to_s + '.'
  puts 'The largest number of people in a room when a match was reached was ' + people_max.to_s + '.'
end
